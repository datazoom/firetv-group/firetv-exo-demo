package com.datazoom.firetv.collector.exo;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.datazoom.firetv.collector.basecollector.connection_manager.DZBeaconConnector;
import com.datazoom.firetv.collector.basecollector.event_collector.collector.DZEventCollector;
import com.datazoom.firetv.collector.basecollector.model.DatazoomConfig;
import com.datazoom.collector.exo.ExoPlayerCollector;
import com.datazoom.firetv.collector.basecollector.model.dz_events.Event;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getCanonicalName();
    PlayerView simpleExoPlayerView;
    ProgressBar pbBuffering;
    Button btnSubmit,btnPush;
//    btnMinimumQuality, btnMaximumQuality;
    private SimpleExoPlayer player;
    private TextView txtVersion;
    TrackSelector trackSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pbBuffering = findViewById(R.id.pbBuffering);
        txtVersion = findViewById(R.id.txtVersion);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnPush=findViewById(R.id.btnPush);
        btnPush.setVisibility(View.INVISIBLE);
//        btnMinimumQuality = findViewById(R.id.btnChangeResolutionDown);
//        btnMaximumQuality = findViewById(R.id.btnChangeResolutionUp);
        pbBuffering.setVisibility(View.GONE);
        txtVersion.setText("Demo - " + BuildConfig.VERSION_NAME + " | Library - " + com.datazoom.collector.exo.BuildConfig.VERSION_NAME + " | Framework - " + com.datazoom.collector.exo.BuildConfig.VERSION_NAME);

        simpleExoPlayerView = findViewById(R.id.exoPlayerView);
        simpleExoPlayerView.requestFocus();

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);

        simpleExoPlayerView.setPlayer(player);
        simpleExoPlayerView.setKeepScreenOn(true);


        btnSubmit.setOnClickListener(v -> {
            btnPush.setVisibility(View.VISIBLE);
            btnSubmit.setVisibility(View.INVISIBLE);
            btnSubmit.setEnabled(false);
            pbBuffering.setVisibility(View.VISIBLE);
            String edtUrl = ((EditText) findViewById(R.id.txtUrl)).getText().toString();
            String[] url = edtUrl.split(" configId:");
            String configId = url[1];
            String configUrl = url[0];



            ExoPlayerCollector.create(player, MainActivity.this)
                    .setConfig(new DatazoomConfig(configId, configUrl))
                    .connect(new DZBeaconConnector.ConnectionListener() {

                        @Override
                        public void onSuccess(DZEventCollector dzEventCollector) {
                            try {
                                dzEventCollector.setDatazoomMetadata(new JSONArray( "[{\"customPlayerName\": \"Exo Player Firetv\"},{\"customDomain\": \"devplatform.io\"}]  "));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Event event = new Event("SDKLoaded",new JSONArray());
                            dzEventCollector.addCustomEvent(event);

                            btnPush.setOnClickListener(v -> {

                                try {
                                    Event event1 = new Event("buttonPush",new JSONArray( "[{\"customPlay\": \"true\"},{\"customPause\": \"false\"}] ") );
                                    dzEventCollector.addCustomEvent(event1);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            });

                            pbBuffering.setVisibility(View.GONE);
                            DataSource.Factory dataSourceFactory =
                                    new DefaultDataSourceFactory(MainActivity.this.getApplicationContext(), Util.getUserAgent(MainActivity.this.getApplicationContext(), "Datazoom ExoPlayer Demo"));

                            Uri videoUri = Uri.parse("https://www.radiantmediaplayer.com/media/bbb-360p.mp4");
                            MediaSource mediaSource = buildMediaSource(videoUri);
                            player.prepare(mediaSource);

                        }

                        @Override
                        public void onError(Throwable t) {
                            pbBuffering.setVisibility(View.GONE);
                            btnSubmit.setEnabled(true);
                            showAlert("Error", "Error while creating ExoPlayerConnector, error:" + t.getMessage());
                        }
                    });

        });

//        btnMinimumQuality.setOnClickListener(v -> {
//            DefaultTrackSelector.Parameters currentParameters = ((DefaultTrackSelector) trackSelector).getParameters();
//            // Build the resulting parameters.
//            DefaultTrackSelector.Parameters newParameters = currentParameters
//                    .buildUpon()
//                    .setForceLowestBitrate(true)
//                    .build();
//            ((DefaultTrackSelector) trackSelector).setParameters(newParameters);
//        });
//
//        btnMaximumQuality.setOnClickListener(v -> {
//
//
//            // Build on the current parameters.
//            DefaultTrackSelector.Parameters currentParameters1 = ((DefaultTrackSelector) trackSelector).getParameters();
//            // Build the resulting parameters.
//            DefaultTrackSelector.Parameters newParameters1 = currentParameters1
//                    .buildUpon()
//                    .setForceHighestSupportedBitrate(true)
//                    .build();
//            ((DefaultTrackSelector) trackSelector).setParameters(newParameters1);
//        });

        try {
            LogMonitor.startAppendingLogsTo(new File(getFilesDir(), "datazoom-log.log"));
        } catch (IOException e) {
            Log.e(TAG, "Cannot create file:datazoom-log.log");
        }


        player.getCurrentTrackGroups();

    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("exoplayer-codelab")).
                createMediaSource(uri);
    }

    private void releasePlayer() {
        if (player != null) {
//            updateResumePosition();
            player.stop();
            player.release();
            player = null;
        }
    }

    private void showAlert(String title, String message) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    // continue with delete
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        releasePlayer();
        LogMonitor.stopMonitoring();
    }

    @Override
    public void onPause() {
        super.onPause();
        player.stop();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
