package com.datazoom.firetv.collector.exo;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Dorothy on 23/08/18.
 */

public class LogMonitor {
    private static final String TAG = "LogMonitor";
    private static boolean sIsRunning = false;
    private static LogMonitor instance;
    private HandlerThread mLogReaderThread = new HandlerThread("log-reader");
    private Handler mLogReaderHandler;
    private Handler mMessageHandler;
    private File file;
    private Runnable mLogReader = new Runnable() {
        @Override
        public void run() {
            readLogs();
        }
    };

    private LogMonitor() {

    }

    public static void startAppendingLogsTo(File file) throws IOException {
        if (null == instance) {
            if (file.exists()) {
                file.delete();
            }

            file.createNewFile();
            instance = new LogMonitor();
            instance.file = file;
            sIsRunning = true;
            instance.startLogReader();
        }
    }

    public static void stopMonitoring() {
        sIsRunning = false;
        instance.stopLogReader();
    }

    private void startLogReader() {
        mLogReaderThread.start();
        mLogReaderHandler = new Handler(mLogReaderThread.getLooper());
        mMessageHandler = new Handler(msg -> {
            writeDataToFile((String) msg.obj);
            return true;
        });
        mLogReaderHandler.post(mLogReader);
    }

    private void writeDataToFile(String data) {

        try {
            Writer output = new BufferedWriter(new FileWriter(file, true));
            output.append(data);
            output.close();

        } catch (IOException ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }

    private void stopLogReader() {
        if (mLogReaderHandler != null) {
            mLogReaderHandler.removeCallbacks(mLogReader);
            mLogReaderThread.quit();
            mLogReaderHandler = null;
            mMessageHandler = null;
        }
    }

    private boolean readLogs() {

        Process process = null;
        BufferedReader reader = null;
        boolean ok = true;

        try {

            // clear buffer first
            clearLogcatBuffer();

            String[] args = {"logcat", "-v", "threadtime"};
            process = Runtime.getRuntime().exec(args);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()), 8192);

            Pattern pattern = Pattern.compile("Sending event:(\\d+) to socket, json:(\\{.*\\})");

            while (sIsRunning) {
                final String line = reader.readLine();
                if (line != null) {

                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        mMessageHandler.sendMessage(mMessageHandler.obtainMessage(0, matcher.group(1) + "-> " + matcher.group(2) + "\n\n"));
                    }

                }
            }

        } catch (IOException e) {

            e.printStackTrace();
            ok = false;

        } finally {

            if (process != null) {
                process.destroy();
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN
                    && reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return ok;

    }

    private void clearLogcatBuffer() {
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"logcat", "-c"});
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
