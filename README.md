# mobile-android-exoplayer-demo
This Android project is a sample application to demonstrate the usage of [Datazoom](https://www.datazoom.io/ "Title")'s Android ExoPlayer collector.






## Instructions for QA to add ExoCollector library in your Android application

The Android ExoPlayer framework allows access to the Exoplayer included with the Android operating system. Datazoom’s ExoCollector facilitates exo android applications to send video playback events based on the configuration created in data-pipes.

### Adding dependency to your project


1. Add following aar files in your android studio.

```
https://gitlab.com/datazoom/mobile-android-group/mobile-android-collector-libraries-internal/tree/master/com/datazoom/android/exo-collector/2.8.5
https://gitlab.com/datazoom/mobile-android-group/mobile-android-collector-libraries-internal/tree/master/com/datazoom/android/base-collector/3.10.4
```
2. Add retrofit library to your project if you don't have it already.

```
dependencies {
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.google.code.gson:gson:2.8.5'
}
```
3. Add following compile options if you don't have it already

```
compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
}
```
### Calling ExoCollector with configurations
Use the following code snippet to add ExoCollector to your project.

```
String configId = <configuration id from Datazoom>
String configUrl = <url given by Datazoom>
ExoPlayerConnector.create(player, MainActivity.this)
                    .setConfig(new DatazoomConfig(configId, configUrl))
                    .connect(new DZBeaconConnector.ConnectionListener(){

                        @Override
                        public void onSuccess() {
                            //Everything is setup, connector initialization complete.
                        }
                        
                        @Override
                        public void onError(Throwable t) {
                            //Error occurred while connector initialization.
                        }
 
                    });
```
## Instructions for QA to add ExoCollector library in our Android application

### Running demo application in emulator/mobile device
Follow the steps to run the application in emulator/mobile device

1. Clone the repository

```
git clone https://gitlab.com/datazoom/firetv-group/firetv-exo-demo.git
```
2. Open mobile-android-exoplayer-demo in Android Studio
3. Run the application


### Demo application
A demo application can be found [here](https://gitlab.com/datazoom/firetv-group/firetv-exo-demo). This can be used to test the Datazoom Exo collector classes.

## Instructions for QA team to perform tests
#### Execute the CURL script

Make sure that you have executed the appropriate CURL script so that the player template is uploaded to beacon-engine. You may be able to get this script from the Wiki page of corresponding player. 



#### Downloading the APK

Download the application from the Wiki page of the corresponding player.

You can identify the latest version by looking into the version code.



#### Setting up your device

From your smartphone or tablet running Android 4.0 or higher, go to Settings, scroll down to Security, and select Unknown sources. Selecting this option will allow you to install apps outside of the Google Play store. Depending on your device, you can also choose to be warned before installing harmful apps. This can be enabled by selecting the Verify apps option in the Security settings.

![Screenshot pane](Sideloading-1.png)

On devices running an earlier version of Android, go to Settings, open the Applications option, select Unknown sources, and click OK on the popup alert.

#### Installing the application

Install the downloaded APK into the mobile by following the instructions on the screen. Or you can install it using ADB by following command: -

adb install -r <PATH_TO_APK>

#### Start using the application

Now you can start using the application by clicking the datazoom application icon

Once the application is started, the following screen shall be shown based on the apk you had installed: -

![Screenshot pane](device-screen.png)

The application shall show two input text boxes

1. The first one is to input the url from where the configuration has to be fetched. 
2. The second one is the configuration id which has to be used

Enter the required configuration id and url and click the submit button to use the application for capturing events. (It may take a few minutes initially for the video to load from the internet. Please wait during this time)

### References
https://developer.android.com/guide/topics/media/exoplayer

## Credits

 - Shyam - Developed the base framework that can be used for all the players.
 - Sreekutty - Implemented ExoPlayer connector and this sample application.

## Link to License/Confidentiality Agreement
 Datazoom, Inc ("COMPANY") CONFIDENTIAL
 Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
 herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes
 information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE,
 OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE
 LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.

